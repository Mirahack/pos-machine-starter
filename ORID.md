O:In the morning, we learned about "tasking", in which we learned how to disassemble a complex task and connect it with actual development. In the process of learning 'tasking', we also learned concept maps and used them to decompose tasks. In addition, we also studied the naming conventions of methods and reviewed the basic usage of Git.
In the afternoon, we started learning to draw concept maps and practiced disassembling business code based on them.

R:I feel confused at first but suddenly enlightened.

I:At first, I didn't understand the purpose of splitting tasks because I felt that some businesses were already simple enough to not need to be split anymore. After the teacher's explanation, I understand the true purpose of the teacher's request for us to split tasks: let us practice task splitting through simple tasks, and then handle complex tasks skillfully according to this method.

D:I will practice concept mapping and task splitting methods more in the future, making me more proficient in handling complex business.