package pos.machine;

public class ReceiptItem {
    private String name;
    private Integer quantity;
    private Integer unitPrice;
    private Integer subTotal;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Integer unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Integer subTotal) {
        this.subTotal = subTotal;
    }

    @Override
    public String toString() {
        return "Name: " + name + ", Quantity: " + quantity + ", Unit price: " + unitPrice + " (yuan), Subtotal: " + subTotal + " (yuan)";
    }
}
