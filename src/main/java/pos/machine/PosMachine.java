package pos.machine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItems);
        return renderReceipt(receipt);
    }

    public List<ReceiptItem> decodeToItems(List<String> barcodes) {
        List<Item> items = ItemsLoader.loadAllItems();
        Map<String, Integer> barcodesMap = new HashMap<>();
        convertBarcodesToMap(barcodes, barcodesMap);
        return getReceiptItemsFromDatabase(items, barcodesMap);
    }

    public List<ReceiptItem> getReceiptItemsFromDatabase(List<Item> items, Map<String, Integer> barcodesMap) {
        return items.stream()
                .filter(item -> barcodesMap.containsKey(item.getBarcode()))
                .map(item -> {
                    ReceiptItem receiptItem = new ReceiptItem();
                    receiptItem.setName(item.getName());
                    receiptItem.setQuantity(barcodesMap.get(item.getBarcode()));
                    receiptItem.setUnitPrice(item.getPrice());
                    receiptItem.setSubTotal(receiptItem.getQuantity() * receiptItem.getUnitPrice());
                    return receiptItem;
                })
                .collect(Collectors.toList());
    }

    public void convertBarcodesToMap(List<String> barcodes, Map<String, Integer> barcodesMap) {
        barcodes.forEach(barcode -> {
            if (!barcodesMap.containsKey(barcode)) {
                barcodesMap.put(barcode, 1);
            } else {
                barcodesMap.put(barcode, barcodesMap.get(barcode) + 1);
            }
        });
    }

    public Integer calculateTotalPrice(List<ReceiptItem> receiptItems) {
        Integer totalPrice = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            totalPrice += receiptItem.getSubTotal();
        }
        return totalPrice;
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems) {
        Integer totalPrice = calculateTotalPrice(receiptItems);
        Receipt receipt = new Receipt();
        receipt.setReceiptItems(receiptItems);
        receipt.setTotalPrice(totalPrice);
        return receipt;
    }

    public String generateItemsReceipt(Receipt receipt) {
        StringBuilder stringBuilder = new StringBuilder();
        receipt.getReceiptItems().forEach(receiptItem -> {
            stringBuilder.append(receiptItem.toString()).append("\n");
        });
        return stringBuilder.toString();
    }

    public String generateReceipt(String itemsReceipt, Integer totalPrice) {
        return "***<store earning no money>Receipt***\n" +
                itemsReceipt +
                "----------------------\n" +
                "Total: " + totalPrice + " (yuan)\n" +
                "**********************";
    }

    public String renderReceipt(Receipt receipt) {
        String itemsReceipt = generateItemsReceipt(receipt);
        return generateReceipt(itemsReceipt, receipt.getTotalPrice());
    }

}
